#
# Service Control Policies
#

#
# Basic functionality required by all accounts
#
# This policy is intended for use with the BAA policy, to enable basic
# services that are not covered by the BAA, but that do not handle PHI
#
data "aws_iam_policy_document" "scp_basic" {
  statement {
    effect    = "Allow"
    resources = [ "*" ]
    actions   = [
      "applicationautoscaling:*",
      "applicationdiscovery:*",
      "billing:*",
      "codepipeline:*",
      "costexplorerservice:*",
      "costandusagereport:*",
      "healthapisandnotifications:*",
      "iam:*",
      "logs:*",
      "organizations:*",
      "performanceinsights:*",
      "resourceaccessmanager:*",
      "resourcegrouptaggingapi:*",
      "resourcegroups:*",
      "support:*",
      "trustedadvisor:*",
      "well-architectedtool:*"
    ]
  }
}

#
# Restrict access to USA regions
#
data "aws_iam_policy_document" "scp_usa_only" {
  statement {
    effect    = "Deny"
    resources = [ "*" ]
    actions   = [ "*" ]

    condition {
      test     = "StringNotEqualsIgnoreCase"
      variable = "aws:RequestedRegion"
      values = var.us_regions
    }
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }
}

#
# BAA Policy
#
# Only enable services covered by the BAA (as listed under the "HIPAA
# BAA" tab on https://aws.amazon.com/compliance/services-in-scope/)
#
data "aws_iam_policy_document" "scp_baa" {
  source_json = data.aws_iam_policy_document.scp_usa_only.json

  statement {
    effect    = "Allow"
    resources = [ "*" ]
    actions   = [
      "apigateway:*",
      "amplify:*",
      "appsync:*",
      "athena:*",
      "autoscaling:*",
      "backup:*",
      "batch:*",
      "certificatemanager:*",
      "certificatemanagerprivatecertificateauthority:*",
      "cloudformation:*",
      "cloudfront:*",
      "cloudhsm:*",
      "cloudtrail:*",
      "cloudwatch:*",
      "cloudwatchevents:*",
      "cloudwatchlogs:*",
      "codebuild:*",
      "codecommit:*",
      "codedeploy:*",
      "cognitoidentity:*",
      "cognitosync:*",
      "cognitouserpools:*",
      "comprehendmedical:*",
      "config:*",
      "connect:*",
      "datasync:*",
      "databasemigrationservice:*",
      "directconnect:*",
      "dynamodb:*",
      "ec2:*",
      "elasticbeanstalk:*",
      "elasticcontainerregistry:*",
      "elasticcontainerservice:*",
      "elasticcontainerserviceforkubernetes:*",
      "elasticfilesystem:*",
      "elasticloadbalancing:*",
      "elasticmapreduce:*",
      "elasticsearchservice:*",
      "elementalmediaconnect:*",
      "elementalmediaconvert:*",
      "elementalmedialive:*",
      "fsx:*",
      "firewallmanager:*",
      "freertos:*",
      "glacier:*",
      "globalaccelerator:*",
      "glue:*",
      "greengrass:*",
      "guardduty:*",
      "importexportdiskservice:*",
      "inspector:*",
      "iot:*",
      "keymanagementservice:*",
      "kinesis:*",
      "kinesisanalytics:*",
      "kinesisfirehose:*",
      "kinesisvideostreams:*",
      "lambda:*",
      "mq:*",
      "macie:*",
      "neptune:*",
      "opsworks:*",
      "opsworksconfigurationmanagement:*",
      "polly:*",
      "quicksight:*",
      "rds:*",
      "redshift:*",
      "rekognition:*",
      "robomaker:*",
      "route53:*",
      "route53resolver:*",
      "route53domains:*",
      "s3:*",
      "sns:*",
      "sqs:*",
      "secretsmanager:*",
      "securityhub:*",
      "servermigrationservice:*",
      "serverlessapplicationrepository:*",
      "servicecatalog:*",
      "shield:*",
      "simpleworkflowservice:*",
      "snowball:*",
      "stepfunctions:*",
      "storagegateway:*",
      "systemsmanager:*",
      "transcribe:*",
      "transferforsftp:*",
      "translate:*",
      "waf:*",
      "wafregional:*",
      "workdocs:*",
      "worklink:*",
      "workspaces:*",
      "workspacesapplicationmanager:*",
      "xray:*"
    ]
  }
}

#
# No Changes Policy
#
# Prevent changes to settings made via the org / provisioning
# while allowing master account's terraform IAM user to make
# changes
#
data "aws_iam_policy_document" "scp_org_management" {

  # Do not allow modifications to the roles created by the org

  statement {
    sid       = "ProtectStanfordIAMSettings"
    effect    = "Deny"
    actions   = [
      "iam:AttachRole*",
      "iam:DeleteRole*",
      "iam:DetachRole*",
      "iam:DeleteServiceLinkedRole",
      "iam:PutRole*",
      "iam:UpdateAssumeRole*",
      "iam:UpdateRole*",
      "iam:UpdateRoleDescription",
    ]
    resources = [
      "arn:aws:iam::*:role/AWSServiceRoleForAmazonGuardDuty",
      "arn:aws:iam::*:role/AWSServiceRoleForCloudTrail",
      "arn:aws:iam::*:role/AWSServiceRoleForOrganizations",
      "arn:aws:iam::*:role/AdminAccess",
      "arn:aws:iam::*:role/BillingAccess",
      "arn:aws:iam::*:role/OrganizationAccountAccessRole",
      "arn:aws:iam::*:role/PowerUserAccess",
      "arn:aws:iam::*:role/admin",
      "arn:aws:iam::*:role/aws-service-role/cloudtrail.amazonaws.com/AWSServiceRoleForCloudTrail",
      "arn:aws:iam::*:role/aws-service-role/config.amazonaws.com/AWSServiceRoleForConfig",
      "arn:aws:iam::*:role/aws-service-role/guardduty.amazonaws.com/AWSServiceRoleForAmazonGuardDuty",
      "arn:aws:iam::*:role/aws-service-role/organizations.amazonaws.com/AWSServiceRoleForOrganizations",
      "arn:aws:iam::*:role/aws-service-role/support.amazonaws.com/AWSServiceRoleForSupport",
      "arn:aws:iam::*:role/billing",
      "arn:aws:iam::*:role/operations",
      "arn:aws:iam::*:role/qualys-cloud-view",
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

  # Do not allow modifications to the Stanford IdP configuration

  statement {
    sid       = "ProtectStanfordSAMLSettings"
    effect    = "Deny"
    actions   = [
      "iam:DeleteSAMLProvider",
      "iam:UpdateSAMLProvider"
    ]
    resources = [
      "arn:aws:iam::*:saml-provider/stanford-idp",
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

  # Do not allow accounts to leave the organization

  statement {
    sid       = "ProtectStanfordOrgAccounts"
    effect    = "Deny"
    actions   = [
      "organizations:leave-organization"
    ]
    resources = [ "*" ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

  # Do not allow modifications to VPC flow logs

  statement {
    sid       = "ProtectStanfordVPCFlowLogs"
    effect    = "Deny"
    actions   = [
      "ec2:CreateFlowLogs",
      "ec2:DeleteFlowLogs",
    ]
    resources = [ "*" ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
    condition {
      test     = "StringEquals"
      variable = "ec2:ResourceTag/Creator"
      values   = [ "org" ]
    }
  }

  # Do not allow modifications to the account password policy

  statement {
    sid       = "ProtectStanfordPasswordPolicy"
    effect    = "Deny"
    actions   = [
      "iam:DeleteAccountPasswordPolicy",
      "iam:UpdateAccountPasswordPolicy",
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

  # Do not allow modifications to GuardDuty settings

  statement {
    sid       = "ProtectStanfordGuardDutySettings"
    effect    = "Deny"
    actions   = [
      "guardduty:Decline*",
      "guardduty:Delete*",
      "guardduty:Disassociate*",
      "guardduty:Stop*",
      "guardduty:Update*",
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

  # Do not allow modifications to Config settings

  statement {
    sid       = "ProtectStanfordConfigSettings"
    effect    = "Deny"
    actions   = [
      "config:Delete*",
      "config:Put*",
      "config:Stop*",
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }

}

#
# Deleted OU Policy
#
# No access for accounts in the deleted OU
#
data "aws_iam_policy_document" "scp_deleted" {
  statement {
    effect    = "Deny"
    actions   = [
      "*"
    ]
    resources = [
      "*"
    ]
    condition {
      test     = "StringNotLike"
      variable = "aws:PrincipalARN"
      values   = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
    }
  }
}

# since SCPs have a size limit, use jsondecode/jsonencode to remove
# unneccesary whitespace
locals {
  scp_baa            = jsonencode(jsondecode(data.aws_iam_policy_document.scp_baa.json))
  scp_basic          = jsonencode(jsondecode(data.aws_iam_policy_document.scp_basic.json))
  scp_usa_only       = jsonencode(jsondecode(data.aws_iam_policy_document.scp_usa_only.json))
  scp_org_management = jsonencode(jsondecode(data.aws_iam_policy_document.scp_org_management.json))
  scp_deleted        = jsonencode(jsondecode(data.aws_iam_policy_document.scp_deleted.json))
}

# ----- Service Control Policies -----

#
# Basic - allow basic AWS services that all accounts need
#
resource "aws_organizations_policy" "basic" {
  name = "basic"
  description = "Basic services"
  content = local.scp_basic
}

#
# BAA - allow AWS services covered by the BAA
#
resource "aws_organizations_policy" "baa" {
  name = "baa"
  description = "Allow BAA covered services"
  content = local.scp_baa
}

#
# USA Only - restrict services to US regions
#
resource "aws_organizations_policy" "usa_only" {
  name = "usa_only"
  description = "Only services in US regions"
  content = local.scp_usa_only
}

#
# No Org Changes - restrict changes to resources created as part of the org
#
resource "aws_organizations_policy" "org_management" {
  name = "org_management"
  description = "Policy to limit management to master account terraform"
  content = local.scp_org_management
}

#
# Deleted - No access for accounts in the deleted OU
#
resource "aws_organizations_policy" "deleted" {
  name = "deleted"
  description = "No access for deleted accounts"
  content = local.scp_deleted
}

output "scp_baa" {
  value = data.aws_iam_policy_document.scp_baa.json
}

output "scp_basic" {
  value = data.aws_iam_policy_document.scp_basic.json
}

output "scp_usa_only" {
  value = data.aws_iam_policy_document.scp_usa_only.json
}

output "scp_org_management" {
  value = data.aws_iam_policy_document.scp_org_management.json
}
