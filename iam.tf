# ----- IAM -----

# ----- Config Service -----

resource "aws_iam_role" "org_config" {
  name = "org_config"
  assume_role_policy = data.aws_iam_policy_document.config_assume_role.json
}

resource "aws_iam_role_policy_attachment" "org_configorg" {
  role       = aws_iam_role.org_config.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSConfigRoleForOrganizations"
}

resource "aws_iam_role_policy_attachment" "org_config" {
  role       = aws_iam_role.org_config.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSConfigRole"
}

# ----- Cross Account Access for People -----

#
# Group for people with admin access to all accounts
#
resource "aws_iam_group" "org_account_access" {
  name = "org-account-access"
}

#
# Policy for admin access to all accounts
#
resource "aws_iam_policy" "org_account_access" {
  name = "org-account-access"
  policy = data.aws_iam_policy_document.org_account_access.json
}

#
# Group / Policy attachment
#
resource "aws_iam_group_policy_attachment" "org_account_access" {
  group =      aws_iam_group.org_account_access.name
  policy_arn = aws_iam_policy.org_account_access.arn
}

#
# IAM users can be added to the group to grant them access
#
resource "aws_iam_group_membership" "org_account_access" {
  name = "org_account_access_membership"
  group = aws_iam_group.org_account_access.name
  users = [
  ]
}

# ----- Billing Integration IAM user -----

resource "aws_iam_user" "billing-automation" {
  name = "billing-automation"
}

#resource "aws_iam_access_key" "billing-automation" {
#  user = aws_iam_user.billing-automation.name
#  pgp_key = var.iam_gpg_key
#}

# ----- Terraform -----

resource "aws_iam_group" "terraform" {
  name = "terraform"
}

#
# Terraform members need full admin access
#
resource "aws_iam_group_policy_attachment" "terraform" {
  group      = aws_iam_group.terraform.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

#
# Terraform user
#
resource "aws_iam_user" "terraform" {
  name = "terraform"
}

#
# Add terraform user to terraform group
#
resource "aws_iam_user_group_membership" "terraform" {
  user   = aws_iam_user.terraform.name
  groups = [ aws_iam_group.terraform.name ]
}

#
# Add terraform role for access from audit account using instance profile
#
resource "aws_iam_role" "terraform" {
  name               = "terraform"
  assume_role_policy = data.aws_iam_policy_document.audit_assume_role.json
}

resource "aws_iam_role_policy_attachment" "terraform_admin" {
  role       = aws_iam_role.terraform.name
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_iam_role_policy_attachment" "terraform_org_access" {
  role       = aws_iam_role.terraform.name
  policy_arn = aws_iam_policy.org_account_access.arn
}

