locals {
  org_id                 = aws_organizations_organization.main.id
  master_id              = data.aws_caller_identity.master.account_id
}

locals {
  tfstate_bucket_name    = format("%s-tfstate", local.master_id)
  onboard_bucket_name    = format("%s-onboard", local.org_id)
  log_bucket_name        = format("%s-logs", local.org_id)
  cloudtrail_bucket_name = format("%s-cloudtrail", local.org_id)
}

