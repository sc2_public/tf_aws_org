# S3 bucket for logs
resource "aws_s3_bucket" "logs" {
  bucket = local.log_bucket_name
  acl    = "log-delivery-write"

  lifecycle_rule {
    id      = "autoclean"
    enabled = true
    abort_incomplete_multipart_upload_days = 2

    expiration {
      days = 180
    }
  }

  tags   = {
    Name   = local.log_bucket_name
    backup = "daily14"
  }
}

resource "aws_s3_bucket_public_access_block" "logs" {
  bucket = aws_s3_bucket.logs.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket" "tfstate" {
  bucket = local.tfstate_bucket_name
  acl    = "private"

  versioning {
    enabled    = true
    mfa_delete = false
  }

  tags   = {
    Name   = local.tfstate_bucket_name
    backup = "daily14"
  }
}

resource "aws_s3_bucket_public_access_block" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}


# S3 bucket for onboarding
resource "aws_s3_bucket" "onboard" {
  bucket = local.onboard_bucket_name
  acl    = "public-read"

  website {
    index_document = "index.html"
    error_document = "error.html"
  }

  logging {
    target_bucket = aws_s3_bucket.logs.id
    target_prefix = "onboard/"
  }

  versioning {
    enabled = true
  }

  tags   = {
    Name   = local.onboard_bucket_name
    backup = "daily14"
  }
}

resource "aws_s3_bucket_public_access_block" "onboard" {
  bucket = aws_s3_bucket.onboard.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_object" "add_org_role" {
  bucket       = aws_s3_bucket.onboard.id
  key          = "add-org-role.json"
  acl          = "public-read"
  content      = data.template_file.add_org_role.rendered
  content_type = "application/json"
  etag         = md5(data.template_file.add_org_role.rendered)
}


