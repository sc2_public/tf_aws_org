variable "aws_profile" {
  description = "Profile to use for AWS CLI commands"
  type        = string
  default     = "uit-itlab"
}

variable "full_access_policy_id" {
  description = "ID of FullAWSAccess SCP"
  type        = string
  default     = "p-FullAWSAccess"
}

variable "iam_gpg_key" {
  type        = string
  description = "Key for encrypting secrets: either base64 encoded GPG public key, or keybase:username"
}

variable "audit_id" {
  type        = string
  description = "12 digit AWS Account ID for audit account"
}

variable "us_regions" {
  type        = list
  description = "List of US regions"
  default     = [
    "us-east-1",
    "us-east-2",
    "us-west-1",
    "us-west-2"
  ]
}

provider "aws" {
  alias = "audit"
}
