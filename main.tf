#
# Master account - for existing orgs, it should be imported with this
# command
#
# $ terraform import aws_organizations_organization.main o-1234567
#
# replace o-1234567 with the organization ID
#
resource "aws_organizations_organization" "main" {
  aws_service_access_principals = [
    "cloudtrail.amazonaws.com",
    "config.amazonaws.com",
    "config-multiaccountsetup.amazonaws.com",
    "tagpolicies.tag.amazonaws.com",
  ]
  enabled_policy_types = [
    "SERVICE_CONTROL_POLICY",
    "TAG_POLICY",
  ]
  feature_set = "ALL"
}

# ----- Organizational Units -----

#
# Default OU
#
# Accounts have FullAWSAccess policy
#
resource "aws_organizations_organizational_unit" "default" {
  name = "default"
  parent_id = aws_organizations_organization.main.roots.0.id
}

resource "aws_organizations_policy_attachment" "default_full_access" {
  policy_id = var.full_access_policy_id
  target_id = aws_organizations_organizational_unit.default.id
}

resource "aws_organizations_policy_attachment" "default_org_management" {
  policy_id = aws_organizations_policy.org_management.id
  target_id = aws_organizations_organizational_unit.default.id
}

#
# Admin - OU for accounts that manage the org
#
# Accounts have FullAWSAccess policy
#
resource "aws_organizations_organizational_unit" "admin" {
  name = "admin"
  parent_id = aws_organizations_organization.main.roots.0.id
}

resource "aws_organizations_policy_attachment" "admin_full_access" {
  policy_id = var.full_access_policy_id
  target_id = aws_organizations_organizational_unit.admin.id
}

#
# HighRisk Default - default OU for accounts containing high risk data
#
# Accounts have FullAWSAccess policy
#
resource "aws_organizations_organizational_unit" "highrisk_default" {
  name = "highrisk-default"
  parent_id = aws_organizations_organization.main.roots.0.id
}

resource "aws_organizations_policy_attachment" "highrisk_default_full_access" {
  policy_id = var.full_access_policy_id
  target_id = aws_organizations_organizational_unit.highrisk_default.id
}

resource "aws_organizations_policy_attachment" "highrisk_default_org_management" {
  policy_id = aws_organizations_policy.org_management.id
  target_id = aws_organizations_organizational_unit.highrisk_default.id
}


#
# PHI Default - default OU for accounts containing PHI
#
# Accounts have BAA policy (which includes Basic and USA Only policies)
#
# NOTE: the FullAccess SCP has to be manually removed from this OU
#
resource "aws_organizations_organizational_unit" "phi_default" {
  name = "phi-default"
  parent_id = aws_organizations_organization.main.roots.0.id
}

resource "aws_organizations_policy_attachment" "phi_default_basic" {
  policy_id = aws_organizations_policy.basic.id
  target_id = aws_organizations_organizational_unit.phi_default.id
}

resource "aws_organizations_policy_attachment" "phi_default_baa" {
  policy_id = aws_organizations_policy.baa.id
  target_id = aws_organizations_organizational_unit.phi_default.id
}

resource "aws_organizations_policy_attachment" "phi_default_org_management" {
  policy_id = aws_organizations_policy.org_management.id
  target_id = aws_organizations_organizational_unit.phi_default.id
}

#
# deleted - OU for accounts that should be deleted
#
# Accounts only have Basic policy
#
# NOTE: the FullAccess SCP has to be manually removed from this OU
#
resource "aws_organizations_organizational_unit" "deleted" {
  name = "accounts for deletion"
  parent_id = aws_organizations_organization.main.roots.0.id
}

resource "aws_organizations_policy_attachment" "deleted_deleted" {
  policy_id = aws_organizations_policy.deleted.id
  target_id = aws_organizations_organizational_unit.deleted.id
}

resource "aws_organizations_policy_attachment" "deleted_org_management" {
  policy_id = aws_organizations_policy.org_management.id
  target_id = aws_organizations_organizational_unit.deleted.id
}

provider "aws" {
  alias = "us_east_1"
}

provider "aws" {
  alias = "us_east_2"
}

provider "aws" {
  alias = "us_west_1"
}

provider "aws" {
  alias = "us_west_2"
}
