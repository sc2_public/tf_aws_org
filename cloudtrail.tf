// ----- Cloudtrail -----

#
# Master cloudtrail setup for all
#
resource "aws_cloudtrail" "master" {
  name = "master"
  s3_bucket_name = data.aws_s3_bucket.cloudtrail.id
  enable_logging = true
  include_global_service_events = true
  is_multi_region_trail = true
  is_organization_trail = true
  enable_log_file_validation = true
}

