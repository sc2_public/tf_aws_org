data "aws_caller_identity" "master" {}

data "aws_availability_zones" "all" {}

data "aws_organizations_organization" "main" {}

data "aws_region" "current" {}

data "aws_s3_bucket" "cloudtrail" {
  bucket   = local.cloudtrail_bucket_name
  provider = aws.audit
}

data "aws_iam_policy_document" "org_account_access" {
  statement {
    actions   = [ "sts:AssumeRole" ]
    resources = [ "arn:aws:iam::*:role/OrganizationAccountAccessRole" ]
  }
}

data "aws_iam_policy_document" "config_assume_role" {
  statement {
    actions = [ "sts:AssumeRole" ]
    principals {
      type = "Service"
      identifiers = [ "config.amazonaws.com" ]
    }

  }
}

#
# Assume role policy to let audit account EC2 instances manage org
#
data "aws_iam_policy_document" "audit_assume_role" {
  statement {

    actions = [
      "sts:AssumeRole"
    ]

    principals {
      type = "AWS"
      identifiers = [
        "arn:aws:iam::${var.audit_id}:root",
        "arn:aws:iam::${var.audit_id}:role/terraformer",
        "arn:aws:sts::${var.audit_id}:role/terraformer",
        "arn:aws:sts::${var.audit_id}:assumed-role/terraformer/terraformer"
      ]
    }
  }
}

data "template_file" "add_org_role" {
  template = file("${path.module}/add_org_role.json.tpl")
  vars = {
    master_id = data.aws_caller_identity.master.account_id
  }
}

