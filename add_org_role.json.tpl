{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Resources": {
    "AddOrganizationAccountAccessRole": {
      "Type": "AWS::IAM::Role",
      "Properties": {
        "RoleName": "OrganizationAccountAccessRole",
        "AssumeRolePolicyDocument": {
          "Statement": [
            {
              "Effect":    "Allow",
              "Principal": { "AWS": "${master_id}" },
              "Action":    [ "sts:AssumeRole" ]
            }
          ]
        },
        "Policies": [
          {
            "PolicyName": "AdministratorAccess",
            "PolicyDocument": {
              "Statement": [
                {
                  "Effect":   "Allow",
                  "Action":   "*",
                  "Resource": "*"
                }
              ]
            }
          }
        ]
      }
    }
  }
}

