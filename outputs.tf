# Admin OU

output "admin_ou_arn"         { value = aws_organizations_organizational_unit.admin.arn  }
output "admin_ou_id"          { value = aws_organizations_organizational_unit.admin.id   }
output "admin_ou_name"        { value = aws_organizations_organizational_unit.admin.name }

# Default OU

output "default_ou_arn"         { value = aws_organizations_organizational_unit.default.arn  }
output "default_ou_id"          { value = aws_organizations_organizational_unit.default.id   }
output "default_ou_name"        { value = aws_organizations_organizational_unit.default.name }


# High Risk OU

output "highrisk_default_ou_arn"         { value = aws_organizations_organizational_unit.highrisk_default.arn  }
output "highrisk_default_ou_id"          { value = aws_organizations_organizational_unit.highrisk_default.id   }
output "highrisk_default_ou_name"        { value = aws_organizations_organizational_unit.highrisk_default.name }

# PHI OU

output "phi_default_ou_arn"         { value = aws_organizations_organizational_unit.phi_default.arn  }
output "phi_default_ou_id"          { value = aws_organizations_organizational_unit.phi_default.id   }
output "phi_default_ou_name"        { value = aws_organizations_organizational_unit.phi_default.name }

# OrgAccountAccess group

output "org_account_access_group_arn"  { value = aws_iam_group.org_account_access.arn  }
output "org_account_access_group_id"   { value = aws_iam_group.org_account_access.id   }
output "org_account_access_group_name" { value = aws_iam_group.org_account_access.name }

# Onboarding

output "onboard_quickcreate" {
  value = format("https://%s.console.aws.amazon.com/cloudformation/home?region=%s#/stacks/quickcreate?templateUrl=%s&stackName=AddOrgRole",
                 data.aws_region.current.name,
                 data.aws_region.current.name,
                 urlencode(format("https://s3-%s.amazonaws.com/%s/%s",
                                  data.aws_region.current.name,
                                  aws_s3_bucket.onboard.id,
                                  aws_s3_bucket_object.add_org_role.key)))
}
